/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

import java.util.ArrayList;

/**
 *
 * @author Juha
 */
public abstract class Package {
    String name;
    String size;
    String mass;
    ArrayList<String> route = new ArrayList();
    boolean breakable;
    int classValue;
    
    public ArrayList<String> getRoute() {
        return route;
    }
    public int getValue() {
        return classValue;
    }
    public String getName() {
        return name;
    }
}
//below inherited classes
class class1 extends Package {
    public class1(String a, String b, String c, String d, boolean e, String f, String g, String h) {
        classValue = 1;
        route.add("'" + a + "'"); //Add "'" for the document.createPath(arraylist, string, int) script in gui
        route.add("'" + b + "'");
        route.add("'" + c + "'");
        route.add("'" + d + "'");
        breakable = e;
        name = f;
        size = g;
        mass = h;
    }
}
class class2 extends Package {
    public class2(String a, String b, String c, String d, String f, String g, String h) {
        classValue = 2;
        route.add("'" + a + "'"); //Add "'" for the document.createPath(arraylist, string, int) script in gui
        route.add("'" + b + "'");
        route.add("'" + c + "'");
        route.add("'" + d + "'");
        breakable = false;
        name = f;
        size = g;
        mass = h;
    }
}
class class3 extends Package {
    public class3(String a, String b, String c, String d, boolean e, String f, String g, String h) {
        classValue = 3;
        route.add("'" + a + "'"); //Add "'" for the document.createPath(arraylist, string, int) script in gui
        route.add("'" + b + "'");
        route.add("'" + c + "'");
        route.add("'" + d + "'");
        breakable = e;
        name = f;
        size = g;
        mass = h;
    }
}
