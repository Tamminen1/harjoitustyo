/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Juha
 */
public class Parse {
    private Document doc;
    private HashMap<String, ArrayList<String>> map;
    private ArrayList<String> arrayCode = new ArrayList();
    private ArrayList<String> arrayCity = new ArrayList();
    private ArrayList<String> arrayAddress = new ArrayList();
    private ArrayList<String> arrayAvailability = new ArrayList();
    private ArrayList<String> arrayPost = new ArrayList();
    private ArrayList<String> arrayLat = new ArrayList();
    private ArrayList<String> arrayLng = new ArrayList();
    
    public HashMap<String, ArrayList<String>> getMap() {
        return map;
    }
    
    public Parse(String content) {
        try { //create new map and new document
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            
            doc.getDocumentElement().normalize();
            
            map = new HashMap();
            parseCurrentData();
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(Parse.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void parseCurrentData() {
        NodeList nodes = doc.getElementsByTagName("place"); //get nodes
        
        for(int i = 0; i < nodes.getLength(); i++) { //parse all nodes and add them to arraylists
            Node node = nodes.item(i); //slice node into smaller nodes
            Element e = (Element) node; //turn nodes into elements
            arrayCode.add(getValue("code", e)); //add the right elements into arraylists
            arrayCity.add(getValue("city", e));
            arrayAddress.add(getValue("address", e));
            arrayAvailability.add(getValue("availability", e));
            arrayPost.add(getValue("postoffice", e));
            arrayLat.add(getValue("lat", e));
            arrayLng.add(getValue("lng", e));
        }
        map.put("Code", arrayCode); //put all arraylists in map
        map.put("City", arrayCity);
        map.put("Address", arrayAddress);
        map.put("Availability", arrayAvailability);
        map.put("Post", arrayPost);
        map.put("Lat", arrayLat);
        map.put("Lng", arrayLng);
    }
    
    private String getValue(String tag, Element e) { //searches the correct element from node for arraylist
        return e.getElementsByTagName(tag).item(0).getTextContent();
    }
}
