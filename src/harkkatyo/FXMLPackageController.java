/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Juha
 */
public class FXMLPackageController implements Initializable {

    @FXML
    private ComboBox<String> objects;
    @FXML
    private TextField name;
    @FXML
    private TextField size;
    @FXML
    private TextField mass;
    @FXML
    private CheckBox breakable;
    @FXML
    private ComboBox<String> start1;
    @FXML
    private ComboBox<String> start2;
    @FXML
    private ComboBox<String> end1;
    @FXML
    private ComboBox<String> end2;
    @FXML
    private Button cancel;
    @FXML
    private Button create;
    @FXML
    private RadioButton class1;
    @FXML
    private RadioButton class2;
    @FXML
    private RadioButton class3;
    Main d1 = new Main();
    private int x = 0;
    private int x2;
    private int x3;
    private int x4;
    private String a;
    private String b;
    private String c;
    private boolean d;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            start1.getItems().addAll(d1.getCity());
            end1.getItems().addAll(d1.getCity());
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        objects.getItems().addAll("Pieni lasiveistos","Kahvakuula","Työkalupakki","Rubikinkuutio");
    }

    @FXML
    private void infoButton(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLInfo.fxml"));
        Stage stage = new Stage();
        stage.setMaxHeight(400);
        stage.setMaxWidth(400);
        stage.setMinHeight(400);
        stage.setMinWidth(400);
        //stage.setTitle("My New Stage Title");
        stage.setScene(new Scene(root, 400, 400));
        stage.show();
    }

    @FXML
    private void searchA(ActionEvent event) {
        start2.getItems().clear();
        start2.getItems().addAll(d1.getAuto(start1.getValue()));
    }

    @FXML
    private void searchB(ActionEvent event) {
        end2.getItems().clear();
        end2.getItems().addAll(d1.getAuto(end1.getValue()));
    }

    @FXML
    private void createPackage(ActionEvent event) throws IOException {
        x2 = 0;
        if(class1.isSelected() == true && start2.getValue() != null && end2.getValue() != null) {
            if(d1.test(start2.getValue(),end2.getValue()) == 1) { //test if class1 is within 150km
                x2 = 1;
            }
            else {
                x2 = 0;
            }
            x = 1;
        }
        else if(class2.isSelected() == true) {
            x = 2;
        }
        else if(class3.isSelected() == true) {
            x = 3;
        }
        else {
            x = 0;
        }
        if(objects.getValue() == null) {
            x3 = 0;
            if(d1.sizeTest(x, size.getText()) == 1) { //size test
                x3 = 1;
            }
            x4 = 0;
            if(d1.massTest(x, mass.getText()) == 1) { //mass test
                x4 = 1;
            }
        }
        if(objects.getValue() != null && start2.getValue() != null && end2.getValue() != null) { //get preset objects
            if(objects.getValue() == "Pieni lasiveistos") { //preset values
                a = "Pieni lasiveistos";
                b = "30 10 10";
                c = "2";
                d = true;
            }
            if(objects.getValue() == "Kahvakuula") {
                a = "Kahvakuula";
                b = "10 10 10";
                c = "10";
                d = false;
            }
            if(objects.getValue() == "Työkalupakki") {
                a = "Työkalupakki";
                b = "40 20 20";
                c = "10";
                d = false;
            }
            if(objects.getValue() == "Rubikinkuutio") {
                a = "Rubikinkuutio";
                b = "10 10 10";
                c = "1";
                d = false;
            }
            if(x > 1) {
                d1.createPackage(start1.getValue(),start2.getValue(),end1.getValue(),end2.getValue(),d,x,objects.getValue(),a,b,c);
            }
            else if(x == 1 && d1.test(start2.getValue(),end2.getValue()) == 0) {
            //send all package info to main class and create class
            d1.createPackage(start1.getValue(),start2.getValue(),end1.getValue(),end2.getValue(),d,x,objects.getValue(),a,b,c);
            }
            else {
                Parent root = FXMLLoader.load(getClass().getResource("FXMLError.fxml"));
                Stage stage = new Stage();
                stage.setMaxHeight(250);
                stage.setMaxWidth(440);
                stage.setMinHeight(250);
                stage.setMinWidth(440);
                stage.setScene(new Scene(root, 250, 440));
                stage.show();
            }
        }
        else if(x != 0 && x2 == 0 && x3 == 0 && x4 == 0 && start2.getValue() != null && end2.getValue() != null) {
            //send all package info to main class and create package
            d1.createPackage(start1.getValue(),start2.getValue(),end1.getValue(),end2.getValue(),breakable.isSelected(),x,objects.getValue(),name.getText(),size.getText(),mass.getText());
        }
        else { // if all the tests are not passed, then show error window for user
            Parent root = FXMLLoader.load(getClass().getResource("FXMLError.fxml"));
            Stage stage = new Stage();
            stage.setMaxHeight(250);
            stage.setMaxWidth(440);
            stage.setMinHeight(250);
            stage.setMinWidth(440);
            stage.setScene(new Scene(root, 250, 440));
            stage.show();
        }
    }
       
}
