/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

import java.util.Arrays;
import java.util.List;


/**
 *
 * @author Juha
 */
public class Test {
    private int x;
    private float dlon;
    private float dlat;
    private String latlon;
    private double d;
    private double dmax = 150;
    private float lat = (float)110.574;
    private float lon;
            
    public int Test(String lat1, String lon1, String lat2, String lon2) { //test if route is less than 150km
        lon = ((float)111.320)*(float)Math.cos(Math.toRadians(Double.parseDouble(lat1)));
        dlon = Float.parseFloat(lon2) - Float.parseFloat(lon1);
        dlat = Float.parseFloat(lat2) - Float.parseFloat(lat1);
        dlat = dlat*lat;
        dlon = dlon*lon;
        latlon = Float.toString(dlat*dlat+dlon*dlon);
        d = Math.sqrt(Double.parseDouble(latlon));
        x = 1;
        if(d<dmax) {
            x = 0;
        }
        return x;
    }
    
    public int sizeTest(int cl, String size) { //test size by class
        x = 0;
        if(cl == 1) {
            List<String> items = Arrays.asList(size.split(" "));
            for(int i=0; i<2; i++) {
                if(Float.parseFloat(items.get(i))>(float)100) {
                    x = 1;
                    break;
                }
            }
        }
        if(cl == 2) {
            List<String> items = Arrays.asList(size.split(" "));
            for(int i=0; i<2; i++) {
                if(Float.parseFloat(items.get(i))>(float)40) {
                    x = 1;
                    break;
                }
            }
        }
        if(cl == 3) {
            List<String> items = Arrays.asList(size.split(" "));
            for(int i=0; i<2; i++) {
                if(Float.parseFloat(items.get(i))>(float)100) {
                    x = 1;
                    break;
                }
            }
        }
        return x;
    }
    
    public int massTest(int cl, String mass) { //test mass by class
        x = 0;
        if(cl == 1) {
            if(Float.parseFloat(mass)>(float)20) {
                x = 1;
            }
        }
        if(cl == 2) {
            if(Float.parseFloat(mass)>(float)10) {
                x = 1;
            }
        }
        if(cl == 3) {
            if(Float.parseFloat(mass)>(float)50) {
                x = 1;
            }
        }
        return x;
    }
}
