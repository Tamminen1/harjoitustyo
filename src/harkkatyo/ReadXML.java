/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 *
 * @author Juha
 */
public class ReadXML {
    private final ArrayList<Place> places = new ArrayList();
    private ArrayList<String> city = new ArrayList();
    private ArrayList<String> address = new ArrayList();
    private ArrayList<String> post = new ArrayList();
    private ArrayList<String> auto = new ArrayList();
    private ArrayList<String> latlng = new ArrayList();
    
    public ArrayList<Place> getXML() throws MalformedURLException, IOException {
        URL url = new URL(" http://smartpost.ee/fi_apt.xml"); //get url
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
       
        String content="";
        String line;
        
        while((line = br.readLine()) != null) { // Read all smartpost lines and put them in content
            content += line + "\n";           
        }
        
        Parse d1 = new Parse(content); //parse content
        for(int i=0; i<479; i++) { //add all parsed data to arraylist
            places.add(new Place(d1.getMap().get("Code").get(i),d1.getMap().get("City").get(i).toUpperCase(),d1.getMap().get("Address").get(i),d1.getMap().get("Availability").get(i),d1.getMap().get("Post").get(i),d1.getMap().get("Lat").get(i),d1.getMap().get("Lng").get(i)));
        }
        return places;
    }
    
    public ArrayList<String> getCity() throws IOException { //returns all citys
        getXML();
        city.add("ÄÄNEKOSKI"); // Add first city manually, because it was written with small letters
        for(int i=1; i<479; i++) { // Add other cities
            if(city.get(city.size()-1).equals(places.get(i).getCity()) == true) { // If the city is the same as the last one -> don't add  
                continue;
            }
            city.add(places.get(i).getCity());
        }
        return city;
    }
    
    public ArrayList<String> getAddress(String cit) { //returns full addresses for a city, also saves postoffice + availability strings
        address.clear();        
        post.clear();
        for(int i=0; i<479; i++) {
            if(cit.equals(places.get(i).getCity()) == true) {
                address.add(places.get(i).getAddress() + ", " + places.get(i).getCode() + " " + places.get(i).getCity());
                post.add(places.get(i).getPost() + ", " + places.get(i).getAvailability());  
            }
        }
        return address;
    }
    
    public ArrayList<String> getPost() { //returns postoffice + availability strings, that were created above
        return post;
    }
    
    public ArrayList<String> getAuto(String cit) { //returns street addresses for a city
        auto.clear();
        for(int i=0; i<479; i++) {
            if(cit.equals(places.get(i).getCity()) == true) {
                auto.add(places.get(i).getAddress());
            }
        }
        return auto;
    }
    
    public ArrayList<String> getLatLng(String add) { //returns latitude and longitude for an address
        latlng.clear();
        for(int i=0; i<479; i++) {
            if(add.equals(places.get(i).getAddress()) == true) {
                latlng.add(places.get(i).getLat());
                latlng.add(places.get(i).getLng());
                break;
            }
        }
        return latlng;
    }
}
