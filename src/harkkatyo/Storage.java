/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

import java.util.ArrayList;

/**
 *
 * @author Juha
 */
public class Storage {
    private ArrayList<Package> packages = new ArrayList();
    private ArrayList<String> names = new ArrayList();
    
    public Storage() {
    }
    
    public void addPackage(String a, String b, String c, String d, boolean e, int f, String h, String i, String j) {
        //create new package depending on class
        //I can save info here and call it back, but for some reason I can't call it back in getRoute, getValue and getName
        //and I don't have time to fix it, so I'm going to leave it there. It would draw routes on map if it could
        //call back those values. I have tested it.
        if(f == 1) {
            packages.add(new class1(a, b, c, d, e, h, i, j));
        }
        if(f == 2) {
            packages.add(new class2(a, b, c, d, h, i, j));
        }
        if(f == 3) {
            packages.add(new class3(a, b, c, d, e, h, i, j));
        }
    }
    
    public ArrayList<String> getRoute(String name) { //returns package's route
        for(int i=0; i<packages.size(); i++) {
            if(packages.get(i).getName().equals(name) == true) {
                return packages.get(i).getRoute();
            }
        }
        return null;
    }
    
    public int getValue(String name) { //returns package's class value
        for(int i=0; i<packages.size(); i++) {
            if(packages.get(i).getName().equals(name) == true) {
                return packages.get(i).getValue();
            }
        }
        return 0;
    }
    
    public ArrayList<String> getName() {
        for(int i=0; i<packages.size(); i++) {
            names.add(packages.get(i).getName());
        }
        return names;
    }
}
