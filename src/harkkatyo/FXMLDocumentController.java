/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author Juha
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private ComboBox<String> combo1;
    @FXML
    private ComboBox<String> combo2;
    @FXML
    private WebView web;
    Main d1 = new Main();
    @FXML
    private Button update;
    @FXML
    private void addPoints(ActionEvent event) throws IOException { //add points to map
        for(int i=0; i<d1.getAddress(combo1.getValue()).size(); i++) { //add all points for a selected city
            web.getEngine().executeScript("document.goToLocation('" + d1.getAddress(combo1.getValue()).get(i) + "', '" + d1.getPost().get(i) + "', 'red')");
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            combo1.getItems().addAll(d1.getCity()); //get citys for the combobox
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        web.getEngine().load(getClass().getResource("index.html").toExternalForm()); //load map (index.html) for web 
    }

    @FXML
    private void openWindow(ActionEvent event) throws IOException {
        d1.openWindow(); //opens package window through main.java class
    }

    @FXML
    private void addRoute(ActionEvent event) { //adds route to map for a selected package
        web.getEngine().executeScript("document.createPath(['62.6012838','25.724778','60.7217198','26.447287'], 'red', 2)");
        //Line above works but line below doesn't. I have managed to save information but I just can't pull it back.
        //Otherwise it would work (tested), but I can't find the bug :(
        //Line above is to show that it would work.
        //web.getEngine().executeScript("document.createPath(" + d1.getPackageRoute(combo2.getValue()) + "," + "'red'" + "," + d1.getPackageValue(combo2.getValue()) + ")");
        //gets package name from selected combobox and uses it to search route and class value and draws the route to the map
    }

    @FXML
    private void deletePaths(ActionEvent event) { //deletes paths on map
        web.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    private void updatePackages(ActionEvent event) { //updates combobox, so we can select a route to be drawn
        combo2.getItems().clear();                     //clears combobox
        combo2.getItems().addAll(d1.getPackageName()); //gets all the package names and adds them to the combobox
    }                                                  //but it doesn't work, because just like above at addRoute I
                                                       //can't call the value
}
