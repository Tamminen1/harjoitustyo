/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

/**
 *
 * @author Juha
 */
public class Place {
    private final String code;
    private final String city;
    private final String address;
    private final String availability;
    private final String post;
    private final String lat;
    private final String lng;
    //class used to save data inside arraylist
    public Place(String a, String b, String c, String d, String e, String f, String g) {
        code = a;
        city = b;
        address = c;
        availability = d;
        post = e;
        lat = f;
        lng = g;
    }
    public String getCode() {
        return code;
    }
    public String getCity() {
        return city;
    }
    public String getAddress() {
        return address;
    }
    public String getAvailability() {
        return availability;
    }
    public String getPost() {
        return post;
    }
    public String getLat() {
        return lat;
    }
    public String getLng() {
        return lng;
    }
}
