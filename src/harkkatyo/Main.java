/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

import java.io.IOException;
import java.util.ArrayList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Juha
 */
public class Main {
    ReadXML d1 = new ReadXML();
    Storage d2 = new Storage();
    Test d3 = new Test();
    private String a1;
    private String a2;
    private String a3;
    private String a4;
    private int test1;
    private int test2;
    private int test3;
    
    
    public ArrayList<String> getCity() throws IOException { //get citys from ReadXML
        return d1.getCity();
    }
    
    public ArrayList<String> getAddress(String cit) { //get full addresses from ReadXML
        return d1.getAddress(cit);
    }
    
    public ArrayList<String> getPost() { //get postoffice + availability string from ReadXML, used for point drawing
        return d1.getPost();
    }
    
    public ArrayList<String> getAuto(String cit) { //get only street addresses from ReadXML
        return d1.getAuto(cit);
    }
    
    public void openWindow() throws IOException { //opens package window
        Parent root = FXMLLoader.load(getClass().getResource("FXMLPackage1.fxml"));
        Stage stage = new Stage();
        stage.setMaxHeight(580);
        stage.setMaxWidth(600);
        stage.setMinHeight(580);
        stage.setMinWidth(600);
        stage.setScene(new Scene(root, 580, 600));
        stage.show();
    }
    
    public void createPackage(String a, String b, String c, String d, boolean e, int f, String g, String h, String i, String j) {
        a1 = d1.getLatLng(b).get(0); //get geo points
        a2 = d1.getLatLng(b).get(1);
        a3 = d1.getLatLng(d).get(0);
        a4 = d1.getLatLng(d).get(1);
        d2.addPackage(a1, a2, a3, a4, e, f, h, i, j); //create package and store it in Storage
        
    }
    
    public ArrayList<String> getPackageRoute(String name) {
        ArrayList<String> x = d2.getRoute(name); //get geo points from Storage = route to be drawn
        return x;
    }
    
    public int getPackageValue(String name) {
        int x2 = d2.getValue(name); //get class value (1,2,3) from Storage
        return x2;
    }
    
    public ArrayList<String> getPackageName() {
        ArrayList<String> x3 = d2.getName();
        return x3;
    }
    
    public int test(String a, String b) { //get class 1 distance test value (is route < 150km or not)
        test1 = d3.Test(d1.getLatLng(a).get(0), d1.getLatLng(a).get(1), d1.getLatLng(b).get(0), d1.getLatLng(b).get(1));
        return test1;
    }
    
    public int sizeTest(int cl, String size) { //get size test value
        test2 = d3.sizeTest(cl, size);
        return test2;
    }
    
    public int massTest(int cl, String mass) { //get mass test value
        test3 = d3.massTest(cl, mass);
        return test3;
    }
}
